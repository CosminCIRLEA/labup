Rails.application.routes.draw do
  resources :users

  root 'sessions#new'
  get 'login' => 'sessions#new', as: 'login'
  post 'login' => 'sessions#create'
end
