class SessionsController < ApplicationController
  def new
  end


    def create
      user = User.find_by(email: params[:email]) # search for the user by email in the DB
      if user && user.authenticate(params[:password]) # checks if users was fouund and if the password match
        session[:user_id] = user.id # sets the id of the user in session
        redirect_to users_path, notice: 'Logged in!' # redirects to /users (index page of users)
      else
        flash.now.alert = 'Email or password is invalid'
        render :new
      end
    end


  def destroy
  end
end